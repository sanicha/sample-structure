using Microsoft.AspNetCore.Mvc;

namespace SampleStructure.Controllers
{
    using Models.WeatherForecast;
    using SampleStructure.Models.Common;
    using SampleStructure.Services.WeatherForecast;
    using Swashbuckle.AspNetCore.Annotations;

    [ApiController]
    [Route("[controller]/[action]")]
    public class WeatherForecastController : ControllerBase
    {
        private const string MODULE = "Weather Forecast";
        private readonly ILogger<WeatherForecastController> _logger;
        private IWeatherForecast _weatherForecast;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IWeatherForecast weatherForecast)
        {
            _logger = logger;
            _weatherForecast = weatherForecast;
        }

        [HttpPost(Name = "GetWeatherForecast")]
        [SwaggerOperation(Tags = new[] { MODULE })]
        [ProducesResponseType(typeof(ResponseModel<WeatherForecastPaginatedResponseModel>), 200)]
        public IActionResult Get(WeatherForecastPaginatedRequestModel data)
        {
            try
            {
                return Ok(_weatherForecast.GetWeatherForecasts(data));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, "Unknown exception occurred");
            }
        }
        [HttpGet(Name = "GetWeatherForecast")]
        [SwaggerOperation(Tags = new[] { MODULE })]
        [ProducesResponseType(typeof(ResponseModel<IEnumerable<WeatherForecastModel>>), 200)]
        public IActionResult Get()
        {
            try
            {
                return Ok(_weatherForecast.GetWeatherForecasts());
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, "Unknown exception occurred");
            }
        }
        [HttpPost(Name = "AddWeatherForecast")]
        [SwaggerOperation(Tags = new[] { MODULE })]
        [ProducesResponseType(typeof(ResponseModel<int>), 200)]
        public IActionResult Add(WeatherForecastModel data)
        {
            try
            {
                if (!data.IsValid())
                {
                    return BadRequest("Invalid data");
                }
                return Ok(_weatherForecast.AddWeatherForecast(data));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500,"Unknown exception occurred");
            }
        }

        [HttpPost(Name = "EditWeatherForecast")]
        [SwaggerOperation(Tags = new[] { MODULE })]
        [ProducesResponseType(typeof(ResponseModel<int>), 200)]
        public IActionResult Edit(WeatherForecastModel data)
        {
            try
            {
                if (!data.IsValid())
                {
                    return BadRequest("Invalid data");
                }
                return Ok(_weatherForecast.EditWeatherForecast(data));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, "Unknown exception occurred");
            }
        }

        [HttpDelete(Name = "DeleteWeatherForecast")]
        [SwaggerOperation(Tags = new[] { MODULE })]
        [ProducesResponseType(typeof(ResponseModel<int>), 200)]
        public IActionResult Delete(int data)
        {
            try
            {
                return Ok(_weatherForecast.EdiDeleteWeatherForecast(data));
            }
            catch (Exception e)
            {
                _logger.LogError(e, e.Message);
                return StatusCode(500, "Unknown exception occurred");
            }
        }
    }
}
