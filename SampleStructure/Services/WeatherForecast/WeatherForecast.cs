﻿using SampleStructure.Models.Common;
using SampleStructure.Models.WeatherForecast;

namespace SampleStructure.Services.WeatherForecast
{

    public class WeatherForecast : IWeatherForecast
    {
        public ResponseModel<WeatherForecastPaginatedResponseModel> GetWeatherForecasts(WeatherForecastPaginatedRequestModel data)
        {
            #region > Data sorting & paging
            var dataPaging = new SampleStructure.Database.Access.DataPagingModel()
            {
                FirstRowNumber = data.RequestedPageSize > 0 ? (data.RequestedPageIndex * data.RequestedPageSize) : 0,
                RequestedRowsCount = data.RequestedPageSize
            };

            string sortFieldName = "";
            if (!string.IsNullOrWhiteSpace(data.SortFieldName))
            {
                switch (data.SortFieldName.ToLower())
                {
                    case "temperaturec":
                        sortFieldName = "[TemperatureC]";
                        break;
                    default:
                    case "date":
                        sortFieldName = "[Date]";
                        break;
                }
            }

            #endregion

            var results = SampleStructure.Database.Access.Tables.WeatherDataAccess.Get( sortFieldName, data.SortItemsDesc, data.RequestedPageIndex, data.RequestedPageSize);
            var allCount = SampleStructure.Database.Access.Tables.WeatherDataAccess.Get_Count();

            //- 
            var responseBody = new WeatherForecastPaginatedResponseModel();

            responseBody.ItemsCount = allCount;
            responseBody.PagesCount = (int)Math.Ceiling((decimal)allCount / (data.RequestedPageSize <= 0 ? 1 : data.RequestedPageSize));
            responseBody.RequestedPageSize = data.RequestedPageSize;
            responseBody.RequestedPageIndex = data.RequestedPageIndex;
            responseBody.Items = results?.Select(x => new WeatherForecastModel (x))?.ToList()
                 ?? new List<WeatherForecastModel>();


            // -
            return ResponseModel<WeatherForecastPaginatedResponseModel>.SuccessResponse(responseBody);
        }
        public ResponseModel<IEnumerable<WeatherForecastModel>> GetWeatherForecasts()
        {
            return ResponseModel<IEnumerable<WeatherForecastModel>>.SuccessResponse(
                (SampleStructure.Database.Access.Tables.WeatherDataAccess.Get()
                        ?? new List<Database.Entities.Tables.WeatherDataEntity>())
                        .Select(x => new WeatherForecastModel(x))
                        .ToList());
        }
        public ResponseModel<int> AddWeatherForecast(WeatherForecastModel data) 
        {
            #region BL validation
            // Business logic data validation here, if needed - like data uniqueness based on some columns like Name, reference, ...
            #endregion BL validation

            // -
            return ResponseModel<int>.SuccessResponse( SampleStructure.Database.Access.Tables.WeatherDataAccess.Insert(data.ToEntity()));
        }
        public ResponseModel<int> EditWeatherForecast(WeatherForecastModel data) 
        {
            #region BL validation
            // Business logic data validation here, if needed - like data uniqueness based on some columns like Name, reference, ...
            var oldEntity = SampleStructure.Database.Access.Tables.WeatherDataAccess.Get(data.Id);
            if (oldEntity is null)
            {
                return ResponseModel<int>.FailureResponse("Forecast Weather does not exist");
            }
            #endregion BL validation

            // -
            var entity = data.ToEntity();
            entity.Id = oldEntity.Id;
            return ResponseModel<int>.SuccessResponse(SampleStructure.Database.Access.Tables.WeatherDataAccess.Update(entity));
        }
        public ResponseModel<int> EdiDeleteWeatherForecast(int id)
        {
            #region BL validation
            // Business logic data validation here, if needed - like data uniqueness based on some columns like Name, reference, ...
            var entity = SampleStructure.Database.Access.Tables.WeatherDataAccess.Get(id);
            if (entity is null)
            {
                return ResponseModel<int>.FailureResponse("Forecast Weather does not exist");
            }
            #endregion BL validation

            // -
            return ResponseModel<int>.SuccessResponse(SampleStructure.Database.Access.Tables.WeatherDataAccess.Delete(id));
        }
    }
}
