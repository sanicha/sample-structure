﻿using SampleStructure.Models.Common;
using SampleStructure.Models.WeatherForecast;

namespace SampleStructure.Services.WeatherForecast
{
    public interface IWeatherForecast
    {
        public ResponseModel<WeatherForecastPaginatedResponseModel> GetWeatherForecasts(WeatherForecastPaginatedRequestModel data);
        public ResponseModel<IEnumerable<WeatherForecastModel>> GetWeatherForecasts();
        public ResponseModel<int> AddWeatherForecast(WeatherForecastModel data);
        public ResponseModel<int> EditWeatherForecast(WeatherForecastModel data);
        public ResponseModel<int> EdiDeleteWeatherForecast(int id);
    }
}
