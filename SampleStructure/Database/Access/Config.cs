using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace SampleStructure.Database.Access
{
    public interface IConfig
    {
    }
    public class Config : IConfig
    {
        public const int MAX_BATCH_SIZE = 1000;
        public static string _connectionString { get; set; } = "";
        public Config(string connectionString)
        {
            _connectionString = connectionString;
        }

        public static string GetConnectionString()
        {
            return _connectionString;
        }
    }
}
