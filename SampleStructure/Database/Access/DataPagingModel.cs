﻿namespace SampleStructure.Database.Access
{
    public class DataPagingModel
    {
        public int FirstRowNumber { get; set; }
        public int RequestedRowsCount { get; set; }
    }

    public class DataSortingModel
    {
        public string SortField { get; set; } = "";
        public bool SortDesc { get; set; }
    }
}
