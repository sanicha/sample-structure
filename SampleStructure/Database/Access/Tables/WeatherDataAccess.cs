using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Microsoft.Data.SqlClient;

namespace SampleStructure.Database.Access.Tables
{
    public class WeatherDataAccess
    {
        #region Default Methods
        public static SampleStructure.Database.Entities.Tables.WeatherDataEntity Get(int id)
        {
            var dataTable = new DataTable();
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                string query = "SELECT * FROM [WeatherData] WHERE [Id]=@Id";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", id);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);

            }

            if (dataTable.Rows.Count > 0)
            {
                return new SampleStructure.Database.Entities.Tables.WeatherDataEntity(dataTable.Rows[0]);
            }
            else
            {
                return null;
            }
        }

        public static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> Get()
        {
            var dataTable = new DataTable();
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                string query = "SELECT * FROM [WeatherData]";
                var sqlCommand = new SqlCommand(query, sqlConnection);

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.Rows.Cast<DataRow>().Select(x => new SampleStructure.Database.Entities.Tables.WeatherDataEntity(x)).ToList();
            }
            else
            {
                return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
            }
        }
        public static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> Get(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                int maxQueryNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE;
                List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> results = null;
                if (ids.Count <= maxQueryNumber)
                {
                    results = get(ids);
                }
                else
                {
                    int batchNumber = ids.Count / maxQueryNumber;
                    results = new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results.AddRange(get(ids.GetRange(i * maxQueryNumber, maxQueryNumber)));
                    }
                    results.AddRange(get(ids.GetRange(batchNumber * maxQueryNumber, ids.Count - batchNumber * maxQueryNumber)));
                }
                return results;
            }
            return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
        }
        private static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> get(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                var dataTable = new DataTable();
                using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
                {
                    sqlConnection.Open();
                    var sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConnection;

                    string queryIds = string.Empty;
                    for (int i = 0; i < ids.Count; i++)
                    {
                        queryIds += "@Id" + i + ",";
                        sqlCommand.Parameters.AddWithValue("Id" + i, ids[i]);
                    }
                    queryIds = queryIds.TrimEnd(',');

                    sqlCommand.CommandText = $"SELECT * FROM [WeatherData] WHERE [Id] IN ({queryIds})";
                    new SqlDataAdapter(sqlCommand).Fill(dataTable);
                }

                if (dataTable.Rows.Count > 0)
                {
                    return dataTable.Rows.Cast<DataRow>().Select(x => new SampleStructure.Database.Entities.Tables.WeatherDataEntity(x)).ToList();
                }
                else
                {
                    return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
                }
            }
            return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
        }

        public static int Insert(SampleStructure.Database.Entities.Tables.WeatherDataEntity item)
        {
            int response = int.MinValue;
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                var sqlTransaction = sqlConnection.BeginTransaction();

                string query = "INSERT INTO [WeatherData] ([Date],[Summary],[TemperatureC]) OUTPUT INSERTED.[Id] VALUES (@Date,@Summary,@TemperatureC); ";

                using (var sqlCommand = new SqlCommand(query, sqlConnection, sqlTransaction))
                {

                    sqlCommand.Parameters.AddWithValue("Date", item.Date == null ? (object)DBNull.Value : item.Date);
                    sqlCommand.Parameters.AddWithValue("Summary", item.Summary == null ? (object)DBNull.Value : item.Summary);
                    sqlCommand.Parameters.AddWithValue("TemperatureC", item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);

                    var result = sqlCommand.ExecuteScalar();
                    response = result == null ? int.MinValue : int.TryParse(result.ToString(), out var insertedId) ? insertedId : int.MinValue;
                }
                sqlTransaction.Commit();

                return response;
            }
        }
        public static int Insert(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items)
        {
            if (items != null && items.Count > 0)
            {
                int maxParamsNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE / 4; // Nb params per query
                int results = 0;
                if (items.Count <= maxParamsNumber)
                {
                    results = insert(items);
                }
                else
                {
                    int batchNumber = items.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results += insert(items.GetRange(i * maxParamsNumber, maxParamsNumber));
                    }
                    results += insert(items.GetRange(batchNumber * maxParamsNumber, items.Count - batchNumber * maxParamsNumber));
                }
                return results;
            }

            return -1;
        }
        private static int insert(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items)
        {
            if (items != null && items.Count > 0)
            {
                int results = -1;
                using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
                {
                    sqlConnection.Open();
                    string query = "";
                    var sqlCommand = new SqlCommand(query, sqlConnection);

                    int i = 0;
                    foreach (var item in items)
                    {
                        i++;
                        query += " INSERT INTO [WeatherData] ([Date],[Summary],[TemperatureC]) VALUES ( "

                            + "@Date" + i + ","
                            + "@Summary" + i + ","
                            + "@TemperatureC" + i
                            + "); ";


                        sqlCommand.Parameters.AddWithValue("Date" + i, item.Date == null ? (object)DBNull.Value : item.Date);
                        sqlCommand.Parameters.AddWithValue("Summary" + i, item.Summary == null ? (object)DBNull.Value : item.Summary);
                        sqlCommand.Parameters.AddWithValue("TemperatureC" + i, item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);
                    }

                    sqlCommand.CommandText = query;

                    results = sqlCommand.ExecuteNonQuery();
                }

                return results;
            }

            return -1;
        }

        public static int Update(SampleStructure.Database.Entities.Tables.WeatherDataEntity item)
        {
            int results = -1;
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                string query = "UPDATE [WeatherData] SET [Date]=@Date, [Summary]=@Summary, [TemperatureC]=@TemperatureC WHERE [Id]=@Id";
                var sqlCommand = new SqlCommand(query, sqlConnection);

                sqlCommand.Parameters.AddWithValue("Id", item.Id);
                sqlCommand.Parameters.AddWithValue("Date", item.Date == null ? (object)DBNull.Value : item.Date);
                sqlCommand.Parameters.AddWithValue("Summary", item.Summary == null ? (object)DBNull.Value : item.Summary);
                sqlCommand.Parameters.AddWithValue("TemperatureC", item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);

                results = sqlCommand.ExecuteNonQuery();
            }

            return results;
        }
        public static int Update(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items)
        {
            if (items != null && items.Count > 0)
            {
                int maxParamsNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE / 4; // Nb params per query
                int results = 0;
                if (items.Count <= maxParamsNumber)
                {
                    results = update(items);
                }
                else
                {
                    int batchNumber = items.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results += update(items.GetRange(i * maxParamsNumber, maxParamsNumber));
                    }
                    results += update(items.GetRange(batchNumber * maxParamsNumber, items.Count - batchNumber * maxParamsNumber));
                }

                return results;
            }

            return -1;
        }
        private static int update(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items)
        {
            if (items != null && items.Count > 0)
            {
                int results = -1;
                using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
                {
                    sqlConnection.Open();
                    string query = "";
                    var sqlCommand = new SqlCommand(query, sqlConnection);

                    int i = 0;
                    foreach (var item in items)
                    {
                        i++;
                        query += " UPDATE [WeatherData] SET "

                            + "[Date]=@Date" + i + ","
                            + "[Summary]=@Summary" + i + ","
                            + "[TemperatureC]=@TemperatureC" + i + " WHERE [Id]=@Id" + i
                            + "; ";

                        sqlCommand.Parameters.AddWithValue("Id" + i, item.Id);
                        sqlCommand.Parameters.AddWithValue("Date" + i, item.Date == null ? (object)DBNull.Value : item.Date);
                        sqlCommand.Parameters.AddWithValue("Summary" + i, item.Summary == null ? (object)DBNull.Value : item.Summary);
                        sqlCommand.Parameters.AddWithValue("TemperatureC" + i, item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);
                    }

                    sqlCommand.CommandText = query;

                    results = sqlCommand.ExecuteNonQuery();
                }

                return results;
            }

            return -1;
        }

        public static int Delete(int id)
        {
            int results = -1;
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                string query = "DELETE FROM [WeatherData] WHERE [Id]=@Id";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.Parameters.AddWithValue("Id", id);

                results = sqlCommand.ExecuteNonQuery();
            }

            return results;
        }
        public static int Delete(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                int maxParamsNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE;
                int results = 0;
                if (ids.Count <= maxParamsNumber)
                {
                    results = delete(ids);
                }
                else
                {
                    int batchNumber = ids.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results += delete(ids.GetRange(i * maxParamsNumber, maxParamsNumber));
                    }
                    results += delete(ids.GetRange(batchNumber * maxParamsNumber, ids.Count - batchNumber * maxParamsNumber));
                }
            }
            return -1;
        }
        private static int delete(List<int> ids)
        {
            if (ids != null && ids.Count > 0)
            {
                int results = -1;
                using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
                {
                    sqlConnection.Open();
                    var sqlCommand = new SqlCommand();
                    sqlCommand.Connection = sqlConnection;

                    string queryIds = string.Empty;
                    for (int i = 0; i < ids.Count; i++)
                    {
                        queryIds += "@Id" + i + ",";
                        sqlCommand.Parameters.AddWithValue("Id" + i, ids[i]);
                    }
                    queryIds = queryIds.TrimEnd(',');

                    string query = "DELETE FROM [WeatherData] WHERE [Id] IN (" + queryIds + ")";
                    sqlCommand.CommandText = query;

                    results = sqlCommand.ExecuteNonQuery();
                }

                return results;
            }
            return -1;
        }

        #region Methods with transaction
        public static SampleStructure.Database.Entities.Tables.WeatherDataEntity GetWithTransaction(int id, SqlConnection connection, SqlTransaction transaction)
        {
            var dataTable = new DataTable();

            string query = "SELECT * FROM [WeatherData] WHERE [Id]=@Id";
            var sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("Id", id);
            new SqlDataAdapter(sqlCommand).Fill(dataTable);

            if (dataTable.Rows.Count > 0)
            {
                return new SampleStructure.Database.Entities.Tables.WeatherDataEntity(dataTable.Rows[0]);
            }
            else
            {
                return null;
            }
        }
        public static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> GetWithTransaction(SqlConnection connection, SqlTransaction transaction)
        {
            var dataTable = new DataTable();

            string query = "SELECT * FROM [WeatherData]";
            var sqlCommand = new SqlCommand(query, connection, transaction);

            new SqlDataAdapter(sqlCommand).Fill(dataTable);

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.Rows.Cast<DataRow>().Select(x => new SampleStructure.Database.Entities.Tables.WeatherDataEntity(x)).ToList();
            }
            else
            {
                return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
            }
        }
        public static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> GetWithTransaction(List<int> ids, SqlConnection connection, SqlTransaction transaction)
        {
            if (ids != null && ids.Count > 0)
            {
                int maxQueryNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE;
                List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> results = null;
                if (ids.Count <= maxQueryNumber)
                {
                    results = getWithTransaction(ids, connection, transaction);
                }
                else
                {
                    int batchNumber = ids.Count / maxQueryNumber;
                    results = new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results.AddRange(getWithTransaction(ids.GetRange(i * maxQueryNumber, maxQueryNumber), connection, transaction));
                    }
                    results.AddRange(getWithTransaction(ids.GetRange(batchNumber * maxQueryNumber, ids.Count - batchNumber * maxQueryNumber), connection, transaction));
                }
                return results;
            }
            return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
        }
        private static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> getWithTransaction(List<int> ids, SqlConnection connection, SqlTransaction transaction)
        {
            if (ids != null && ids.Count > 0)
            {
                var dataTable = new DataTable();

                var sqlCommand = new SqlCommand("", connection, transaction);
                string queryIds = string.Empty;
                for (int i = 0; i < ids.Count; i++)
                {
                    queryIds += "@Id" + i + ",";
                    sqlCommand.Parameters.AddWithValue("Id" + i, ids[i]);
                }
                queryIds = queryIds.TrimEnd(',');

                sqlCommand.CommandText = $"SELECT * FROM [WeatherData] WHERE [Id] IN ({queryIds})";
                new SqlDataAdapter(sqlCommand).Fill(dataTable);

                if (dataTable.Rows.Count > 0)
                {
                    return dataTable.Rows.Cast<DataRow>().Select(x => new SampleStructure.Database.Entities.Tables.WeatherDataEntity(x)).ToList();
                }
                else
                {
                    return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
                }
            }
            return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
        }

        public static int InsertWithTransaction(SampleStructure.Database.Entities.Tables.WeatherDataEntity item, SqlConnection connection, SqlTransaction transaction)
        {
            int response = int.MinValue;


            string query = "INSERT INTO [WeatherData] ([Date],[Summary],[TemperatureC]) OUTPUT INSERTED.[Id] VALUES (@Date,@Summary,@TemperatureC); ";


            var sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("Date", item.Date == null ? (object)DBNull.Value : item.Date);
            sqlCommand.Parameters.AddWithValue("Summary", item.Summary == null ? (object)DBNull.Value : item.Summary);
            sqlCommand.Parameters.AddWithValue("TemperatureC", item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);

            var result = sqlCommand.ExecuteScalar();
            return result == null ? int.MinValue : int.TryParse(result.ToString(), out var insertedId) ? insertedId : int.MinValue;
        }
        public static int InsertWithTransaction(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items, SqlConnection connection, SqlTransaction transaction)
        {
            if (items != null && items.Count > 0)
            {
                int maxParamsNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE / 4; // Nb params per query
                int results = 0;
                if (items.Count <= maxParamsNumber)
                {
                    results = insertWithTransaction(items, connection, transaction);
                }
                else
                {
                    int batchNumber = items.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results += insertWithTransaction(items.GetRange(i * maxParamsNumber, maxParamsNumber), connection, transaction);
                    }
                    results += insertWithTransaction(items.GetRange(batchNumber * maxParamsNumber, items.Count - batchNumber * maxParamsNumber), connection, transaction);
                }
                return results;
            }

            return -1;
        }
        private static int insertWithTransaction(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items, SqlConnection connection, SqlTransaction transaction)
        {
            if (items != null && items.Count > 0)
            {
                string query = "";
                var sqlCommand = new SqlCommand(query, connection, transaction);

                int i = 0;
                foreach (var item in items)
                {
                    i++;
                    query += " INSERT INTO [WeatherData] ([Date],[Summary],[TemperatureC]) VALUES ( "

                        + "@Date" + i + ","
                        + "@Summary" + i + ","
                        + "@TemperatureC" + i
                            + "); ";


                    sqlCommand.Parameters.AddWithValue("Date" + i, item.Date == null ? (object)DBNull.Value : item.Date);
                    sqlCommand.Parameters.AddWithValue("Summary" + i, item.Summary == null ? (object)DBNull.Value : item.Summary);
                    sqlCommand.Parameters.AddWithValue("TemperatureC" + i, item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);
                }

                sqlCommand.CommandText = query;

                return sqlCommand.ExecuteNonQuery();
            }

            return -1;
        }

        public static int UpdateWithTransaction(SampleStructure.Database.Entities.Tables.WeatherDataEntity item, SqlConnection connection, SqlTransaction transaction)
        {
            int results = -1;

            string query = "UPDATE [WeatherData] SET [Date]=@Date, [Summary]=@Summary, [TemperatureC]=@TemperatureC WHERE [Id]=@Id";
            var sqlCommand = new SqlCommand(query, connection, transaction);

            sqlCommand.Parameters.AddWithValue("Id", item.Id);
            sqlCommand.Parameters.AddWithValue("Date", item.Date == null ? (object)DBNull.Value : item.Date);
            sqlCommand.Parameters.AddWithValue("Summary", item.Summary == null ? (object)DBNull.Value : item.Summary);
            sqlCommand.Parameters.AddWithValue("TemperatureC", item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);

            results = sqlCommand.ExecuteNonQuery();
            return results;
        }
        public static int UpdateWithTransaction(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items, SqlConnection connection, SqlTransaction transaction)
        {
            if (items != null && items.Count > 0)
            {
                int maxParamsNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE / 4; // Nb params per query
                int results = 0;
                if (items.Count <= maxParamsNumber)
                {
                    results = updateWithTransaction(items, connection, transaction);
                }
                else
                {
                    int batchNumber = items.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results += updateWithTransaction(items.GetRange(i * maxParamsNumber, maxParamsNumber), connection, transaction);
                    }
                    results += updateWithTransaction(items.GetRange(batchNumber * maxParamsNumber, items.Count - batchNumber * maxParamsNumber), connection, transaction);
                }

                return results;
            }

            return -1;
        }
        private static int updateWithTransaction(List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> items, SqlConnection connection, SqlTransaction transaction)
        {
            if (items != null && items.Count > 0)
            {
                int results = -1;
                string query = "";
                var sqlCommand = new SqlCommand(query, connection, transaction);

                int i = 0;
                foreach (var item in items)
                {
                    i++;
                    query += " UPDATE [WeatherData] SET "

                    + "[Date]=@Date" + i + ","
                    + "[Summary]=@Summary" + i + ","
                    + "[TemperatureC]=@TemperatureC" + i + " WHERE [Id]=@Id" + i
                        + "; ";

                    sqlCommand.Parameters.AddWithValue("Id" + i, item.Id);
                    sqlCommand.Parameters.AddWithValue("Date" + i, item.Date == null ? (object)DBNull.Value : item.Date);
                    sqlCommand.Parameters.AddWithValue("Summary" + i, item.Summary == null ? (object)DBNull.Value : item.Summary);
                    sqlCommand.Parameters.AddWithValue("TemperatureC" + i, item.TemperatureC == null ? (object)DBNull.Value : item.TemperatureC);
                }

                sqlCommand.CommandText = query;
                return sqlCommand.ExecuteNonQuery();
            }

            return -1;
        }

        public static int DeleteWithTransaction(int id, SqlConnection connection, SqlTransaction transaction)
        {
            int results = -1;

            string query = "DELETE FROM [WeatherData] WHERE [Id]=@Id";
            var sqlCommand = new SqlCommand(query, connection, transaction);
            sqlCommand.Parameters.AddWithValue("Id", id);

            results = sqlCommand.ExecuteNonQuery();


            return results;
        }
        public static int DeleteWithTransaction(List<int> ids, SqlConnection connection, SqlTransaction transaction)
        {
            if (ids != null && ids.Count > 0)
            {
                int maxParamsNumber = SampleStructure.Database.Access.Config.MAX_BATCH_SIZE;
                int results = 0;
                if (ids.Count <= maxParamsNumber)
                {
                    results = deleteWithTransaction(ids, connection, transaction);
                }
                else
                {
                    int batchNumber = ids.Count / maxParamsNumber;
                    for (int i = 0; i < batchNumber; i++)
                    {
                        results += deleteWithTransaction(ids.GetRange(i * maxParamsNumber, maxParamsNumber), connection, transaction);
                    }
                    results += deleteWithTransaction(ids.GetRange(batchNumber * maxParamsNumber, ids.Count - batchNumber * maxParamsNumber), connection, transaction);
                }
            }
            return -1;
        }
        private static int deleteWithTransaction(List<int> ids, SqlConnection connection, SqlTransaction transaction)
        {
            if (ids != null && ids.Count > 0)
            {
                int results = -1;

                var sqlCommand = new SqlCommand("", connection, transaction);

                string queryIds = string.Empty;
                for (int i = 0; i < ids.Count; i++)
                {
                    queryIds += "@Id" + i + ",";
                    sqlCommand.Parameters.AddWithValue("Id" + i, ids[i]);
                }
                queryIds = queryIds.TrimEnd(',');

                string query = "DELETE FROM [WeatherData] WHERE [Id] IN (" + queryIds + ")";
                sqlCommand.CommandText = query;

                results = sqlCommand.ExecuteNonQuery();


                return results;
            }
            return -1;
        }
        #endregion Methods with transaction
        #endregion Default Methods

        #region Custom Methods
        // - Paginated get
        public static List<SampleStructure.Database.Entities.Tables.WeatherDataEntity> Get(string sortColumn, bool sortDesc, int requestedPage, int pageSize)
        {
            sortColumn = sortColumn ?? "Id";
            requestedPage = requestedPage < 0 ? 0 : requestedPage;
            pageSize = pageSize <= 0 ? 10 : pageSize;
            var dataTable = new DataTable();
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                string query = "SELECT * FROM [WeatherData]";
                query += $@" ORDER BY {sortColumn} {(sortDesc ? "DESC" : "ASC")} OFFSET {requestedPage * pageSize} ROWS FETCH NEXT {pageSize} ROWS ONLY";
                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.CommandTimeout = 300;

                new SqlDataAdapter(sqlCommand).Fill(dataTable);
            }

            if (dataTable.Rows.Count > 0)
            {
                return dataTable.Rows.Cast<DataRow>().Select(x => new SampleStructure.Database.Entities.Tables.WeatherDataEntity(x)).ToList();
            }
            else
            {
                return new List<SampleStructure.Database.Entities.Tables.WeatherDataEntity>();
            }
        }
        public static int Get_Count()
        {
            using (var sqlConnection = new SqlConnection(SampleStructure.Database.Access.Config.GetConnectionString()))
            {
                sqlConnection.Open();
                string query = "SELECT COUNT(*) FROM [WeatherData]";

                var sqlCommand = new SqlCommand(query, sqlConnection);
                sqlCommand.CommandTimeout = 300;

                return int.TryParse(sqlCommand.ExecuteScalar().ToString(), out var val) ? val : 0;
            }
        }
        #endregion Custom Methods
    }
}
