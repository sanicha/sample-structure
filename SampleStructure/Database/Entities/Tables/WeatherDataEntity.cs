using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace SampleStructure.Database.Entities.Tables
{
    public class WeatherDataEntity
    {
		public DateTime? Date { get; set; }
		public int Id { get; set; }
		public string? Summary { get; set; }
		public decimal? TemperatureC { get; set; }

        public WeatherDataEntity() { }

        public WeatherDataEntity(DataRow dataRow)
        {
			Date = (dataRow["Date"] == System.DBNull.Value) ? (DateTime?)null : Convert.ToDateTime(dataRow["Date"]);
			Id = Convert.ToInt32(dataRow["Id"]);
			Summary = (dataRow["Summary"] == System.DBNull.Value) ? "" : Convert.ToString(dataRow["Summary"]);
			TemperatureC = (dataRow["TemperatureC"] == System.DBNull.Value) ? (decimal?)null : Convert.ToDecimal(dataRow["TemperatureC"]);
        }
    
        public WeatherDataEntity ShallowClone()
        {
            return new WeatherDataEntity
            {
			Date = Date,
			Id = Id,
			Summary = Summary,
			TemperatureC = TemperatureC,
            };
        }
    }
}

