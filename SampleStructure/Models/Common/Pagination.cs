﻿namespace SampleStructure.Models.Common
{
    public class IPaginatedRequestModel
    {
        // - 
        public int RequestedPageIndex { get; set; } = 0;
        public int RequestedPageSize { get; set; } = 10;
        public bool SortItemsDesc { get; set; } = true;
        public string SortFieldName { get; set; } = "";
        // -
        public bool ReturnFullData { get; set; } = false;
    }

    public class IPaginatedResponseModel<T>
    {
        public int ItemsCount { get; set; }
        public int PagesCount { get; set; }
        public int RequestedPageIndex { get; set; }
        public int RequestedPageSize { get; set; }
        public List<T> Items { get; set; } = new List<T>();
    }
}
