﻿using SampleStructure.Models.Common;
using System.ComponentModel.DataAnnotations;

namespace SampleStructure.Models.WeatherForecast
{
    public class WeatherForecastModel
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Date is required")]
        [DataType(DataType.Date, ErrorMessage = "Date invalid value")]
        public DateOnly Date { get; set; }

        [Required(ErrorMessage = "Temperature is required")]
        [Range(-50, 50, ErrorMessage = "Temperature must be between -50 and 50 deg")]
        public decimal TemperatureC { get; set; }
        public decimal TemperatureF => 32 + (TemperatureC / 0.5556m);

        [DataType(DataType.Text)]
        [MaxLength(250, ErrorMessage = "Summary is too long. Max value allowed is 250 chars")]
        public string? Summary { get; set; }


        public WeatherForecastModel()
        {
        }
        public WeatherForecastModel(SampleStructure.Database.Entities.Tables.WeatherDataEntity entity)
        {
            if (entity is null)
            {
                return;
            }

            // - 
            Date =  DateOnly.FromDateTime(entity.Date.HasValue ? entity.Date.Value: DateTime.Now);
            TemperatureC = entity.TemperatureC ?? 0;
            Summary = entity.Summary;
        }
        public SampleStructure.Database.Entities.Tables.WeatherDataEntity ToEntity()
        {
            return new Database.Entities.Tables.WeatherDataEntity
            {
                Id=Id,
                TemperatureC = TemperatureC,
                Summary = Summary,
                Date = Date.ToDateTime(TimeOnly.MinValue)
            };
        }
        public bool IsValid()
        {

            // - Some more logical data validattion here if needed.
            return true;
        }
    }
    public class WeatherForecastPaginatedRequestModel: IPaginatedRequestModel
    {

    }
    public class WeatherForecastPaginatedResponseModel:IPaginatedResponseModel<WeatherForecastModel>
    {

    }
}
